﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Skillz.MailingService.Models.Input
{
    public class SendMailInput
    {
        public List<string> DestMailAddresses { get; set; }
        public string Subject { get; set; }
        public string HtmlBody { get; set; }
    }
}
