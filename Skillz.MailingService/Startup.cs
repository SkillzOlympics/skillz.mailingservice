using Elastic.Apm.AspNetCore;
using Elastic.Apm.DiagnosticSource;
using Skillz.MailingService.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Prometheus;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Skillz.MailingService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers().AddNewtonsoftJson();

            //ToDo: Register Typed Client as explained in https://www.stevejgordon.co.uk/introduction-to-httpclientfactory-aspnetcore
            services.AddHttpClient();

            services.AddCors(options =>
            {
                options.AddPolicy("allowallpolicy",
                builder =>
                {
                    //builder.WithOrigins("http://example.com",
                    //                    "http://www.contoso.com");
                    builder.AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowAnyOrigin();
                });
            });

            services.AddSingleton<SampleService>();


            //filter the json schema
            //https://www.thetopsites.net/article/53319609.shtml
            //https://dejanstojanovic.net/aspnet/2019/october/ignoring-properties-from-controller-action-model-in-swagger-using-jsonignore/

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();

            //Based on solution from here, to enable both Redoc and SwaggerUI
            //http://stevenmaglio.blogspot.com/2019/12/using-swagger-ui-and-redoc-in-aspnet.html

            app.UseReDoc(c =>
            {
                c.SpecUrl = "../swagger/v1/swagger.json";
                //c.RoutePrefix = "";
                c.DocumentTitle = "Mail API Documention";
            });

            app.UseSwaggerUI(c =>
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Mail v1")
            );

            if (Configuration.GetValue<bool>("Prometheus:Enabled"))
            {
                app.UseHttpMetrics();
            }

            if (Configuration.GetValue<bool>("ElasticApm:Enabled"))
            {
                app.UseElasticApm(Configuration, new HttpDiagnosticsSubscriber());
            }

            if (Configuration.GetValue<bool>("Serilog:RequestLogging:Enabled"))
            {
                app.UseSerilogRequestLogging();
            }

            app.UseCors("allowallpolicy");

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapGet("/", async context =>
                {
                    Assembly.GetExecutingAssembly().GetManifestResourceNames();

                    var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("Skillz.MailingService.Resources.index.html");
                    context.Response.ContentLength = stream.Length;
                    context.Response.ContentType = "text/html";
                    var s1 = context.Response.BodyWriter.AsStream();
                    await stream.CopyToAsync(s1);
                });
            });
        }
    }
}
