﻿using Microsoft.Extensions.Logging;
using Serilog.Core;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TraceReloggerLib;

namespace Skillz.MailingService.Config
{
    public class ConfigLogging
    {
        public class SeqConfigLogging
        {
            public bool Enabled { get; set; }
            public string Uri { get; set; }
        }

        public class ElasticConfigLogging
        {
            public bool Enabled { get; set; }
            public string Uri { get; set; }
            public FailuresConfigLogging Failures { get; set; }
        }

        public class FailuresConfigLogging
        {
            public bool Enabled { get; set; }
            public string Uri { get; set; }
        }

        public class FileConfigLogging
        {
            public bool Enabled { get; set; }
            public string Uri { get; set; }
        }

        public class ConsoleConfigLogging
        {
            public bool Enabled { get; set; }
        }
        public LogEventLevel LogLevel { get; set; }
        public SeqConfigLogging Seq { get; set; }
        public ElasticConfigLogging Elastic { get; set; }
        public FileConfigLogging File { get; set; }
        public ConsoleConfigLogging Console { get; set; }

    }
}
