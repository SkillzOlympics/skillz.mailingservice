using Elastic.Apm.SerilogEnricher;
using Elastic.CommonSchema.Serilog;
using Skillz.MailingService.Config;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Core;
using Serilog.Formatting.Compact;
using Serilog.Sinks.Elasticsearch;
using Serilog.Sinks.File;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Skillz.MailingService
{
    public class Program
    {
        //source https://stackoverflow.com/questions/39031585/how-do-i-log-from-other-classes-than-the-controller-in-asp-net-core
        //get logger<T> using service scope
        public static ILogger<T> GetLogger<T>()
        {
            using var serviceScope = _host.Services.CreateScope();
            var services = serviceScope.ServiceProvider;
            return services.GetRequiredService<ILogger<T>>();
        }
        //get logger T via gloval loggerfactory
        public static ILogger<T> GetLogger2<T>()
        {
            return _loggerFactory.CreateLogger<T>();

        }

        static IHost _host;
        static ILoggerFactory _loggerFactory;

        [Obsolete]

        public static async Task<int> Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            //Strong Type Configuration
            IConfigurationRoot configuration = builder.Build();
            ConfigLogging cl = new ConfigLogging();
            configuration.GetSection("Logging").Bind(cl);

            LoggingLevelSwitch ls = new LoggingLevelSwitch(cl.LogLevel);

            var loggerConfig = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .Enrich.WithProperty("ApplicationContext", "Codez.GameRunner")
                .Enrich.FromLogContext()
                .Enrich.WithElasticApmCorrelationInfo()
                .Enrich.WithMachineName()
                .MinimumLevel.ControlledBy(ls);


            if (cl.Console.Enabled)
            {
                loggerConfig.WriteTo.Console(
                    outputTemplate: "{Timestamp:HH:mm:ss} [{Level:u3}] [{SourceContext}] {ElasticApmTraceId} {ElasticApmTransactionId} {Message:lj} {NewLine}{Exception}"
                    );
            }

            if (cl.Seq.Enabled)
            {
                loggerConfig.WriteTo.Seq(cl.Seq.Uri);
            }

            if (cl.File.Enabled)
            {
                loggerConfig.WriteTo.File(new CompactJsonFormatter(), cl.File.Uri, rollingInterval: RollingInterval.Day);
            }

            if (cl.Elastic.Enabled)
            {
                var eso = new ElasticsearchSinkOptions(new Uri("http://localhost:9200"))
                {
                    AutoRegisterTemplate = true,
                    AutoRegisterTemplateVersion = AutoRegisterTemplateVersion.ESv6,
                    CustomFormatter = new EcsTextFormatter(),//new ExceptionAsObjectJsonFormatter(renderMessage: true),
                    IndexFormat = $"{Assembly.GetExecutingAssembly().GetName().Name.ToLower().Replace(".", "-")}-{DateTime.UtcNow:yyyy-MM}",
                    //FailureCallback = e => Console.WriteLine("Unable to submit event " + e.MessageTemplate),
                    // MinimumLogEventLevel = LogEventLevel.Fatal
                };

                if (cl.Elastic.Failures.Enabled)
                {
                    eso.EmitEventFailure = EmitEventFailureHandling.WriteToFailureSink;
                    eso.FailureSink = new FileSink(cl.Elastic.Failures.Uri, new CompactJsonFormatter(), null);
                }


                loggerConfig.WriteTo.Elasticsearch(eso);
            }

            Log.Logger = loggerConfig.CreateLogger();

            ILoggerFactory _loggerFactory = (ILoggerFactory)new LoggerFactory();
            _loggerFactory.AddSerilog();



            try
            {
                Serilog.Log.Information("Starting web host");
                _host = CreateHostBuilder(configuration.GetValue<int>("Port", 16670)).Build();

                var th = _host.RunAsync();
                //empty call to force creation of the NodeManager
                //var cache = (NodeManager)_host.Services.GetService(typeof(NodeManager));
                await th;
                return 0;
            }
            catch (Exception ex)
            {
                Serilog.Log.Fatal(ex, "Host terminated unexpectedly");
                return 1;
            }
            finally
            {
                Serilog.Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(int port/*,ConfigurationManager cm*/) =>
            Host.CreateDefaultBuilder()
                .UseSerilog()
                //https://medium.com/@frankkerrigan/creating-a-service-for-both-linux-systemd-and-windows-service-fe4ddfa68597
                .UseWindowsService()
                .UseSystemd()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseUrls($"http://0.0.0.0:{port}");
                    webBuilder.UseStartup<Startup>();
                });
    }
}
