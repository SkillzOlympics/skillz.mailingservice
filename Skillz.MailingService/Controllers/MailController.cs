﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Net.Mail;
using System.Net;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using Skillz.MailingService.Models.Input;

namespace Skillz.MailingService.Controllers
{
    public class SmsRequest
    {
        public string UserName { get; set; }
        public string EncryptPassword { get; set; }
        public string Subscribers { get; set; }
        public string Message { get; set; }
        public string SenderName { get; set; }
        public int DeliveryDelayInMinutes { get; set; }
        public int ExpirationDelayInMinutes { get; set; }
        public string SendId { get; set; }
    }


    [Route("[controller]")]
    [ApiController]
    public class MailController : ControllerBase
    {
        readonly ILogger<MailController> _logger;
        private readonly IConfiguration _configuration;

        public MailController(IConfiguration configuration)//, ILogger<MailController> logger)
        {
            _configuration = configuration;
            //_logger = logger;
        }

        private bool IsValidMailAddress(string mailAddress)
        {
            // source https://stackoverflow.com/questions/16167983/best-regular-expression-for-email-validation-in-c-sharp
            return Regex.IsMatch(mailAddress, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
        }

        private async Task<bool> SendMailAsync(string destMailAddress, string subject, string htmlBody)
        {
            try
            {
                string sourceMailAddress = _configuration.GetValue<string>("MailAddress");
                string sourceMailPassword = _configuration.GetValue<string>("MailPassword");

                SmtpClient client = new SmtpClient
                {
                    Host = "smtp.office365.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new System.Net.NetworkCredential(sourceMailAddress, sourceMailPassword)
                };
                MailMessage message = new MailMessage(sourceMailAddress, destMailAddress);

                message.Subject = subject;
                // source https://stackoverflow.com/questions/7860218/text-alignment-in-sending-mail
                message.Body = htmlBody;
                message.IsBodyHtml = true;

                await client.SendMailAsync(message);

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("e" + e);

                return false;
            }
        }

        [HttpPost("SendMail")]
        public async Task<ActionResult<bool>> SendMail(SendMailInput input)
        {
            if (input.DestMailAddresses.Any(m => !IsValidMailAddress(m)))
                return BadRequest("Some addresses are not valid mail address");

            return await SendMailAsync(string.Join(',', input.DestMailAddresses), input.Subject, input.HtmlBody);
        }

        [HttpPost("SendSMS")]
        public async Task<ActionResult<bool>> SendSMS(string phone, string msg)//, string requestId)
        {
            bool isPhone = Regex.IsMatch(phone, "^[0-9]{10}$");

            if (isPhone)
            {
                string userName = _configuration.GetValue<string>("SMSUserName");
                string encryptPassword = _configuration.GetValue<string>("SMSEncryptPassword");
                string senderName = _configuration.GetValue<string>("SMSSenderName");

                string SMSMessage = _configuration.GetValue<string>("SMSMessage");

                SmsRequest smsRequest = new SmsRequest()
                {
                    UserName = userName,
                    EncryptPassword = encryptPassword,
                    Subscribers = phone,
                    Message = SMSMessage.Replace("{Message}", msg),
                    SenderName = senderName,
                    DeliveryDelayInMinutes = 0,
                    ExpirationDelayInMinutes = 0,
                    SendId = "1111"
                };

                string json = JsonConvert.SerializeObject(smsRequest);
                Dictionary<string, string> requestData = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

                HttpClient client = new HttpClient();

                var content = new FormUrlEncodedContent(requestData);

                HttpResponseMessage response = await client.PostAsync("https://www.simplesms.co.il/webservice/SmsWS.asmx/SendSms", content);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    return false;
                }

                string resContest = await response.Content.ReadAsStringAsync();

                return true;
            }
            return false;
        }


    }

}
